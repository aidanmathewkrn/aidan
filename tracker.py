

items = dict()
sales = dict()

def custom_sort(dict_to_sort):
    """Custom Sorting"""


def add_item(new_item):
    """Add new item to items dict"""

def del_item(item_name):
    """Delete item from items dict"""

def update_item(updated_values):
    """Update values in items dict"""

def format_dict(headers):
    """Format and print dictionary using python formating"""

def view_items():
    """
    List contents in the item dict
    Headers: Name, Purchase Price, Sales Price, Quantity, Created On, Updated On
    """

def get_sale_uuid():
    """Get unique sale id"""

def add_sale(new_sale):
    """Add new item to sale dict"""

def del_item(sale_uuid):
    """Delete item from sale dict"""

def view_todays_sales():
    """
    List contents in the sale dict
    Headers: Sale ID, Name, Price, Quantity
    """

def sale_summary():
    """
    Get summary of daywise sales.
    Headers: Date, No of Items Sold, Total Amount.
    """
